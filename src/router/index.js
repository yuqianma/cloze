import Vue from 'vue'
import Router from 'vue-router'
import Cloze from '@/components/Cloze'
import ClozeInit from '@/components/ClozeInit'
import ClozeSelect from '@/components/ClozeSelect'
import ClozeTest from '@/components/ClozeTest'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', redirect: { path: '/cloze' } },
    {
      path: '/cloze',
      component: Cloze,
      children: [
        { path: '', redirect: { path: 'init' } },
        { path: 'init', component: ClozeInit },
        { path: 'select', component: ClozeSelect },
        { path: 'test', component: ClozeTest }
      ]
    }
  ]
})
