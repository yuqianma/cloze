class MeasureText {

  constructor (dom) {
    this.canvas = document.createElement('canvas')
    this.ctx = this.canvas.getContext('2d')
    if (dom) {
      const style = window.getComputedStyle(dom)
      this.setStyle(style)
    }
  }

  setStyle (style) {
    this.fontWeight = style.fontWeight
    this.fontSize = style.fontSize
    this.fontFamily = style.fontFamily
    this.ctx.font = [this.fontWeight, this.fontSize, this.fontFamily].join(' ')
    this._styled = true
    return this
  }

  text (t) {
    this._text = t
    return this
  }

  get width () {
    if (!this._styled) {
      console.warn('MeasureText\'s style has not been set, please give it a dom in constructor or use .setStyle()')
    }
    return this.ctx.measureText(this._text).width
  }

}

export default MeasureText
